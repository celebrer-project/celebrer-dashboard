# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from django.utils.translation import ugettext_lazy as _

from horizon import tables
from horizon.utils import filters


def get_updated_time(object):
    return filters.parse_isotime(object.updated_time) or None


class AgentsTable(tables.DataTable):
    node = tables.Column("id", verbose_name=_("Name"))
    status = tables.Column("status", verbose_name=_("Status"))
    services = tables.Column("services", verbose_name=_("Services"))

    class Meta(object):
        name = "agents"
        verbose_name = _("Agents")
        table_actions = (tables.FilterAction,)
