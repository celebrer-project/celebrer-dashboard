from celebrer_dashboard import exceptions

DASHBOARD = 'celebrer'
ADD_INSTALLED_APPS = [
    'celebrer_dashboard'
]
DEFAULT = True

ADD_EXCEPTIONS = {
    'recoverable': exceptions.RECOVERABLE,
    'not_found': exceptions.NOT_FOUND,
    'unauthorized': exceptions.UNAUTHORIZED,
}
