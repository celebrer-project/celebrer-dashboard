# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from django import http
from django import shortcuts
from django.utils.translation import ugettext_lazy as _

from celebrer_dashboard.api import celebrer
from celebrer_dashboard.reports.tables import ReportsTable

from horizon import exceptions
from horizon import tables

def download_report(request):
    task_id = request.path.split('/')[-1]
    response = http.HttpResponse(content_type='application/zip')
    response.write(celebrer.report_download(request, task_id))
    response['Content-Disposition'] = ('attachment; filename="%s.tar.gz"' % task_id)
    return response

class IndexView(tables.DataTableView):
    table_class = ReportsTable
    template_name = 'celebrer/reports/index.html'

    def get_data(self):
        try:
            services = celebrer.reports_list(self.request)
        except Exception:
            services = []
            exceptions.handle(self.request,
                              _('Unable to retrieve reports.'))
        return services
