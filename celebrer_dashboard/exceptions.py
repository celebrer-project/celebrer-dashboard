from openstack_dashboard import exceptions


NOT_FOUND = exceptions.NOT_FOUND
UNAUTHORIZED = exceptions.UNAUTHORIZED
RECOVERABLE = exceptions.RECOVERABLE
