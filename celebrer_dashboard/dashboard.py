from django.utils.translation import ugettext_lazy as _

import horizon


class Celebrer(horizon.Dashboard):
    name = _("Coverage")
    slug = "celebrer"
    panels = ('tasks', 'agents')
    default_panel = 'tasks'


horizon.register(Celebrer)
