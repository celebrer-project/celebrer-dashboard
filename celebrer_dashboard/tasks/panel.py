from django.utils.translation import ugettext_lazy as _

import horizon

from celebrer_dashboard import dashboard


class Tasks(horizon.Panel):
    name = _("Tasks")
    slug = 'tasks'


dashboard.Celebrer.register(Tasks)