from django import http
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from horizon import exceptions
from horizon import tables
from horizon import forms

from celebrer_dashboard.api import celebrer
from celebrer_dashboard.tasks.tables import TasksTable
from celebrer_dashboard.tasks import forms as tasks_forms


def download_report(request):
    task_id = request.path.split('/')[-1]
    binary_report = celebrer.report_download(request, task_id)
    if not len(binary_report):
        raise exceptions.NotFound()
    response = http.HttpResponse(content_type='application/zip')
    response.write(binary_report)
    response['Content-Disposition'] = ('attachment; '
                                       'filename="%s.tar.gz"' % task_id)
    return response


class IndexView(tables.DataTableView):
    table_class = TasksTable
    template_name = 'celebrer/tasks/index.html'
    page_title = _("Tasks")

    def get_data(self):
        try:
            services = celebrer.reports_list(self.request)
        except Exception as inst:
            services = []
            exceptions.handle(self.request,
                              _('Unable to retrieve reports.'
                                'Error: %s' % inst.message))
        return services


class CreateView(forms.ModalFormView):
    form_class = tasks_forms.CreateForm
    form_id = "create_task_form"
    modal_header = _("Create task for collect coverage report")
    submit_label = _("Start")
    submit_url = reverse_lazy('horizon:celebrer:tasks:create')
    template_name = 'celebrer/tasks/form.html'
    context_object_name = 'data'
    success_url = reverse_lazy("horizon:celebrer:tasks:index")