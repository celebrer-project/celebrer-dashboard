from django.conf.urls import patterns
from django.conf.urls import url

from celebrer_dashboard.tasks import views


urlpatterns = patterns(
    '',
    url(r'report_download/[a-f0-9]{32}$', views.download_report, name='download'),
    url(r'^create/$', views.CreateView.as_view(), name='create'),
    url(r'^$', views.IndexView.as_view(), name='index'),
)
