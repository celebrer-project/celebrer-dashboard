from django.utils.translation import pgettext_lazy
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext_lazy

from horizon import tables
from horizon.utils import filters

from celebrer_dashboard.api import celebrer


class DownloadReport(tables.LinkAction):
    name = "download_report"
    verbose_name = _("Download")
    verbose_name_plural = _("Download coverage report")
    icon = "download"

    def get_link_url(self, datum):
        self.url = "/horizon/celebrer/tasks/report_download/%s" % str(self.table.get_object_id(datum))
        return self.url

    def allowed(self, request, task=None):
        if task.report_file:
            return True
        return False


class StopCoverage(tables.BatchAction):
    name = "stop_coverage"
    verbose_name = _("Stop")
    verbose_name_plural = _("Stop collecting report")

    @staticmethod
    def action_present(count):
        return ungettext_lazy(
            u"Stop collecting report",
            u"Stop collecting reports",
            count
        )

    @staticmethod
    def action_past(count):
        return ungettext_lazy(
            u"Collecting report was stopped",
            u"Collecting reports were stopped",
            count
        )

    def allowed(self, request, task=None):
        # ToDo: Later need to add more statuses
        if task.status in ['Scheduled']:
            return True
        return False

    def action(self, request, obj_id):
        celebrer.task_stop(request, obj_id)


class StartCoverage(tables.LinkAction):
    name = "start_coverage"
    verbose_name = _("Run coverage")
    verbose_name_plural = _("Run service with coverage")
    url = "horizon:celebrer:tasks:create"
    classes = ("ajax-modal", "btn-create")
    icon = "plus"
    ajax = True


class UpdateRow(tables.Row):
    ajax = True

    def get_data(self, request, task_id):
        tasks = celebrer.reports_list(request)
        for task in tasks:
            if task.id == task_id:
                return task
        return None

    def load_cells(self, image=None):
        super(UpdateRow, self).load_cells(image)
        # Tag the row with the image category for client-side filtering.
        task = self.datum

class TasksTable(tables.DataTable):
    STATUS_CHOICES = (
        ("Finished", True),
        ("Stoppping", None),
        ("Started", None),
        ("Scheduled", None),
        ("Failure", False),
        ("Generating", None),
    )
    STATUS_DISPLAY_CHOICES = (
        ("Finished", pgettext_lazy("Current status of an Task", u"Finished")),
        ("Stoppping", pgettext_lazy("Current status of an Task", u"Stoppping")),
        ("Started", pgettext_lazy("Current status of an Task", u"Running")),
        ("Scheduled", pgettext_lazy("Current status of an Task",u"Queued")),
        ("Failure", pgettext_lazy("Current status of an Task", u"Failure")),
        ("Generating", pgettext_lazy("Current status of an Task", u"Generating")),
    )
    created = tables.Column("created", verbose_name=_("Created"),
                            filters=[filters.parse_isotime])
    updated = tables.Column("updated", verbose_name=_("Last update"),
                            filters=[filters.parse_isotime])
    # Id
    #id = tables.Column("id", verbose_name=_("ID"))
    # Component (ToDO: need add filter for show services)
    component_name = tables.Column("component_name", verbose_name=_("Component"))
    # Status
    status = tables.Column("status",
                           verbose_name=_("Status"),
                           status=True,
                           status_choices=STATUS_CHOICES,
                           display_choices=STATUS_DISPLAY_CHOICES)
    # Actions

    class Meta(object):
        name = "tasks"
        row_class = UpdateRow
        status_columns = ["status"]
        verbose_name = _("Tasks")
        table_actions = (tables.FilterAction, StartCoverage,)
        row_actions = (DownloadReport, StopCoverage,)
