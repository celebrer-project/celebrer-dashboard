from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from horizon import exceptions
from horizon import forms
from horizon import messages

from celebrer_dashboard.api import celebrer


INDEX_URL = "horizon:celebrer:tasks:index"


class CreateForm(forms.SelfHandlingForm):
    component = forms.ChoiceField(label=_("Component"))

    def __init__(self, request, *args, **kwargs):
        super(CreateForm, self).__init__(request, *args, **kwargs)
        components = celebrer.components_list(request)
        self.fields['component'].choices = [(component.id, component.id)
                                             for component in components]

    def handle(self, request, data):
        try:
            celebrer.run_component(request, data['component'])
            msg = _('Running coverage for "%s" successfully') % data['component']
            messages.success(request, msg)
            return reverse(INDEX_URL)
        except Exception:
            redirect = reverse(INDEX_URL)
            exceptions.handle(request,
                              _("Unable to run coverage."),
                              redirect=redirect)