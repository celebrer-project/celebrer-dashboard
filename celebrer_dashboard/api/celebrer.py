# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
import oslo_utils

from horizon.utils import memoized

from openstack_dashboard.api import base

from celebrerclient import client as celebrer_client


class Agents(base.APIResourceWrapper):
    _attrs = ['id', 'status', 'services']


class Services(base.APIResourceWrapper):
    _attrs = ['id', 'name', 'component', 'status', 'node']


class Components(base.APIResourceWrapper):
    _attrs = ['id']


class Tasks(base.APIResourceWrapper):
    _attrs = ['id', 'created', 'updated', 'status', 'action', 'component_name', 'service_list', 'report_file']


def _get_endpoint(request):
    return base.url_for(request, 'coverage_collector')


@memoized.memoized
def celebrerclient(request):
    endpoint = _get_endpoint(request)
    return celebrer_client.Client(1, endpoint=endpoint)


def services_list(request):
    """Returns all services."""
    services = celebrerclient(request).actions.list_services()
    return [Services(c) for c in services]


def components_list(request):
    """Returns all componentss."""
    components = celebrerclient(request).actions.list_components()
    return [Components(c) for c in components]


def reports_list(request):
    """Returns all tasks."""
    tasks = celebrerclient(request).actions.list_reports()
    for task in tasks:
        task.created = oslo_utils.timeutils.iso8601_from_timestamp(task.created,
                                                                   True)
        task.updated = oslo_utils.timeutils.iso8601_from_timestamp(task.updated,
                                                                   True)
    return [Tasks(c) for c in tasks]


def report_download(request, task_id):
    """Download report"""
    report_response = celebrerclient(request).actions.download_reports(task_id)
    return report_response


def run_component(request, component_name):
    """Returns all components."""
    components = celebrerclient(request).actions.run_component(component_name)


def task_stop(request, task_id):
    """Stop collect coverage report"""
    celebrerclient(request).actions.stop_services(task_id)


def agents_list(request):
    """Returns all agents."""
    agents = celebrerclient(request).actions.list_agents()
    return [Agents(c) for c in agents]
